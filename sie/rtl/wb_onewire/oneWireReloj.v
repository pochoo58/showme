module oneWireReloj #(parameter Counter=185)(/// CLOCK 270KHZ ////
		input clk_in,
		output reg clk_out
		);
		
reg [7:0] counter;

		initial begin
			clk_out <= 0;
			counter <= 0;
		end
		
always@(posedge clk_in)
	begin
		if(counter == Counter)
			begin
			clk_out <= ~clk_out;
			counter = 0;
			end
		else
		counter = counter + 1;
	end

endmodule
