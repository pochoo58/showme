`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:45:30 04/10/2016 
// Design Name: 
// Module Name:    transformacionLetraALetra 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module letraALetra(palabra,reloj,reset,display,letra);

input [63:0] palabra;
input reloj;
input[7:0] display;
input reset;
output [7:0]letra;
reg [7:0]letraAux;
assign letra=letraAux;
always @(display)
case(display)
8'b1111_1110:letraAux=palabra[7:0];
8'b1111_1101:letraAux=palabra[15:8];
8'b1111_1011:letraAux=palabra[23:16];
8'b1111_0111:letraAux=palabra[31:24];
8'b1110_1111:letraAux=palabra[39:32];
8'b1101_1111:letraAux=palabra[47:40];
8'b1011_1111:letraAux=palabra[55:48];
8'b0111_1111:letraAux=palabra[63:56];
endcase
endmodule
