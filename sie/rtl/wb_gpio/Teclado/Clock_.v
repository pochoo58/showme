`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:56:38 11/19/2015 
// Design Name: 
// Module Name:    Clock 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Clock_ #(parameter Counter=185)(/// CLOCK 270KHZ ////
		input clk_in,
		output reg clk_out
		);
		
reg [7:0] counter;

		initial begin
			clk_out <= 0;
			counter <= 0;
		end
		
always@(posedge clk_in)
	begin
		if(counter == Counter)
			begin
			clk_out <= ~clk_out;
			counter = 0;
			end
		else
		counter = counter + 1;
	end

endmodule
