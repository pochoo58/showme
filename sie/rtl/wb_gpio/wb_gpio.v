/*
 *
 * Simple 24-bit wide GPIO module
 * 
 * Can be made wider as needed, but must be done manually.
 * 
 * First lot of bytes are the GPIO I/O regs
 * Second lot are the direction registers
 * 
 * Set direction bit to '1' to output corresponding data bit.
 *
 * Register mapping:
 *  
 * For 8 GPIOs we would have
 * adr 0: gpio data 7:0
 * adr 1: gpio data 15:8
 * adr 2: gpio data 23:16
 * adr 3: gpio dir 7:0
 * adr 4: gpio dir 15:8
 * adr 5: gpio dir 23:16
 * 
 * Backend pinout file needs to be updated for any GPIO width changes.
 * 
 */ 

module wb_gpio(
	    clk,
	    rst,
	    
	    wb_adr_i,
	    wb_dat_i,
	    wb_we_i,
	    wb_cyc_i,
	    wb_stb_i,
	    
	    wb_ack_o,
	    wb_dat_o,
	    gpio_io,

	//variables del teclado		
		clk_in,
		column, //Columnas de la matriz numerica
		files, //Filas de la matriz numerica
		data,

		//variables de Visualización

		numeroInstruccion,
		valor,
		leds,
		display,

		
		// caudal
		sig_a,
		sig_a_risedge,
		contador,

		//motobombas
		activacion,
		calentamiento,
		relojM1,
		relojM2,
		relojM3,
		tempSel,
		//selección del usuario
		profile
);
	input clk;
   input rst;
// caudal
	input sig_a; 
	output sig_a_risedge;
	output [31:0]contador;
	//Teclado
	wire clk_out;
	wire [7:0] value;
	output [7:0] data;
	input clk_in;
	output [3:0]column; //Columnas de la matriz numerica
	input [3:0]files; //Filas de la matriz numerica
	
	//Visualizacion

	input [3:0] numeroInstruccion;
	input [31:0] valor;

	output [6:0] leds;
	output [7:0] display;
	//Motobombas

	input  activacion;
	input  calentamiento;
	input  [5:0]tempSel;
	output relojM1;
	output relojM2;
	output relojM3;

	wire [7:0] data;
	reg [7:0] fr1;
	reg [7:0] fr2;
	reg [7:0] fr3;

	input [3:0] profile;

//------------------ Datos del wb_gpio------------------------
   parameter gpio_io_width = 16;

   parameter gpio_dir_reset_val = 0;
   parameter gpio_o_reset_val = 0;
   //parameter tempSel= 25 ;
   
   parameter wb_dat_width = 32;
   parameter wb_adr_width = 32; // 2^32 bytes addressable

   
   //WishBone Interface
   input [wb_adr_width-1:0] wb_adr_i;
   input [wb_dat_width-1:0] wb_dat_i;
   input 		    wb_we_i;
   input 		    wb_cyc_i;
   input 		    wb_stb_i;
   
   output reg [wb_dat_width-1:0] wb_dat_o; // constantly sampling gpio in bus
   output 		 wb_ack_o;
      
   //I/O PORT
   inout [gpio_io_width-1:0] gpio_io;
   //Interupt


   // Internal registers
   reg [gpio_io_width-1:0]   gpio_dir;

   reg [gpio_io_width-1:0]   gpio_o;

   wire [gpio_io_width-1:0]  gpio_i; 
   
//Wisbone logical Interface

   


   wire wb_rd = wb_stb_i & wb_cyc_i & ~wb_we_i;
   wire wb_wr = wb_stb_i & wb_cyc_i &  wb_we_i;

   reg  ack;
   assign wb_ack_o = wb_stb_i & wb_cyc_i & ack;
    
    
   // Tristate logic for IO
   genvar    i;
   generate 
      for (i=0;i<gpio_io_width;i=i+1)  begin: gpio_tris
	 assign gpio_io[i] = (gpio_dir[i]) ? gpio_o[i] : 1'bz;
	 assign gpio_i[i] = gpio_io[i]; //(gpio_dir[i]) ? gpio_o[i] : ;
	 end
   endgenerate
  //Interupt Mask
/*
  assign interrupt_mask = ~gpio_dir & wb_dat_o;
  
  rising_edge_detect r0(.clk(clk),.signal(interrupt_mask[0]),.pulse(vec_interrupt[0]));
  rising_edge_detect r1(.clk(clk),.signal(interrupt_mask[1]),.pulse(vec_interrupt[1]));
  rising_edge_detect r2(.clk(clk),.signal(interrupt_mask[2]),.pulse(vec_interrupt[2]));
  rising_edge_detect r3(.clk(clk),.signal(interrupt_mask[3]),.pulse(vec_interrupt[3]));
  rising_edge_detect r4(.clk(clk),.signal(interrupt_mask[4]),.pulse(vec_interrupt[4]));
  rising_edge_detect r5(.clk(clk),.signal(interrupt_mask[5]),.pulse(vec_interrupt[5]));
  rising_edge_detect r6(.clk(clk),.signal(interrupt_mask[6]),.pulse(vec_interrupt[6]));
  rising_edge_detect r7(.clk(clk),.signal(interrupt_mask[7]),.pulse(vec_interrupt[7]));
  
  assign irq=|vec_interrupt;
 */ 
   // GPIO data out register
 always @(posedge clk)begin
     if (rst)begin
	///Reset all
        gpio_o <= 0; // All set to in at reset
        gpio_dir <= 0;
        ack <= 0;
     end else begin 
	//No hay reset, el peroferico está activado
	//No hay esclavo seleccionado, señal de acknowledge es cero
	ack<=0;
	//No hay datos de salida (Salidas que van hacia el maestro)
	wb_dat_o[31:28]<= 24'b0;
	//Solicitud de escritura y no hay periférico seleccionado
        if (wb_rd & ~ack) begin             //Read cycle
	//Activar el acknowledge
         ack<=1;
	//Pregunta por la condición de entrada o salida pero solo en las posiciones 3 y 2
         case(wb_adr_i[5:2])
		  4'b0000:begin  //
			wb_dat_o[31:8]<=0;		  //gpio es un cable que se fija con el generate de entrada o salida
							  // esa variable inout es el que las parte en entradas o salidas
							  //wb_dat_o[27:0] <= gpio_i;
			wb_dat_o[7:0] <=gpio_i;//Al parecer solo entra cuando hay solo 00, como indicadores de entradas, para ser leidas en el ciclo de lectura
			end
		4'b1000: wb_dat_o[31:0]<=contador;
		//4'b1001: wb_dat_o[31:0]<=32'd25;	
		4'b1001: wb_dat_o[5:0]<=tempSel;
		4'b1010: wb_dat_o[3:0]<=profile;
         default: wb_dat_o <= 32'b0; 	
         endcase
        end

        else if (wb_wr & ~ack ) begin  
//Activar el acknowledge, para escoger el periférico
            ack <= 1;                          //Write cycle
            case(wb_adr_i[5:2])
		
//Este case atiende dos casos adicionales a los que atendía el caso de lectura
//El primero es de pasar la entrada del wishbone al registro de salida del gpio, que a su vez pasa al registro gpio_io que es un inout del sistema
//             2'b01: gpio_o   <= wb_dat_i[31:0];
		4'b0001: gpio_o <= wb_dat_i[31:0];
             	4'b0010: gpio_dir <= wb_dat_i[31:0];
		4'b0011: fr1 <= wb_dat_i[7:0];
		4'b0100: fr2 <= wb_dat_i[7:0];
		4'b0101: fr3 <= wb_dat_i[7:0];
            endcase
        end
     end        
   end   
  
/*
  always @(posedge clk)begin
    if(cont < 1'b1) cont<=cont+1;
    else begin
    reg_interrupt<=interrupt_mask;
    cont<=0;
    end  
  end

  always @(posedge clk)
  if(interrupt_mask==reg_interrupt) irq<=0;
  else irq<=1;  
*/  


//--------------- Modulos necesarios para el teclado -----------------------------------

Clock_ Clocker(.clk_in(clk_in), //Reloj con el cual va a operar el teclado
					.clk_out(clk_out)//es decir con la frecuencia que registrá los datos
					);

Matrix_Keyboard Matrix_Keyboard (.clk_out(clk_out), //Modulo donde se encuentra la matriz que define los digitos
											.column(column),   // del teclado
											.files(files),
											.value(value)
											);
			 
Register Register (.value(value), // Modulo para guardar de las variables del teclado
						 .clk_out(clk_out), 
						 .data(data)
							);


//--------------- Modulo que permite saber que vamos a mostrar --------------------------------
displaymodule displayModule (.reloj(clk),
									  .reset(rst),
									  .numeroInstruccion(numeroInstruccion),
									  .valor(valor), 
									  .display(display), 
									  .leds(leds)
										);  

//--------------- Modulo para detectar el pulso del caudalimetro--------------------------------
detectorPWM detectorPWM (.sig_a(sig_a), 
		 	.rst(Reset), 
		 	.clk(clk), 
			.sig_a_risedge(sig_a_risedge), 
			.contador(contador),
			.activacion(activacion) );

activacionMotobombas activacionMotobombas1(
	.reloj(clk),
	.reset(rst), 
	.fr1(fr1), 
	.fr2(fr2),
	.activacion(activacion), 
	.calentamiento(calentamiento),
	.relojM1(relojM1), 
	.relojM2(relojM2), 
	.relojM3(relojM3));


endmodule 

