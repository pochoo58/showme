#Carpeta SIE#

En esta carpeta se incluye todo el código utilizado en el proyecto show-me. Esta también se encuentra dividida en subcarpetas, 
cada una de estas incluye código de una sección especial del proyecto

**Cores**

En esta carpeta se añaden los archivos referentes a los protocolos de comunicación ps2, UART y VGA. 
Estos códigos no se utilizaron directamente en la implementación, sin embargo fueron fundamentales 
en las tareas de consulta.

**Firmware**
Aquí se presentan algunas pruebas desarrolladas desde el software. Cada una de los ficheros que
se encuentran ahí contienen su propio makefile y image.ram, lo que permite que su contenido sea
ejecutado de manera independiente.

**RTL**
La carpeta de lenguaje de transferencia de registros contiene información acerca de los diferentes protocolos
usados en el desarrollo del proyecto. En cada una de las carpetas alli contenidas se incluye información acerca 
de las conexiones al wishbone y la interconexión entre las entradas de los perífericos y el protocolo.

Las carpetas más importantes de esta sección corresponden a:
     
     lm32: Contiene la descripción de hardware completa del SOC LM32.
     
     wb_ram: Posee información acerca de la memoria del procesador
     
     wb_conbus: El conbus permite la asignación de las variables de los wb de cada périferico a las entradas del procesador 
     
     wb_gpio: Contiene la conexión al wishbone del GPIO.
     
     wb_onewire: Incluye el código de conexión wishbone al Onewire.
     
     wb_timer: Contiene la conexión al wishbone del timer.
     
     wb_uart: Incluye el código de conexión wishbone al UART.
     